#lang pollen

◊define-meta[title]{Cat and Cast the Spell}

We cast with cat, giving it all our fragments in the order we want them
◊call{cat begin earth water fire air end}

Your terminal will fill with a spell.  Read it aloud to yourself, feel it about yourself.  Well done!  You did this!


