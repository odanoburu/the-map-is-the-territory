#lang pollen

◊define-meta[title]{Goodbye}

You likely wanna take a break from your computer, or you're itching to check your email.  Before this, take a moment to place both of your feet on the ground, and lay your hands flat on the surface of the table/desk/lap your computer is on. Feel all the stuff we kicked up today, all the commands and histories and tech terms and animist spirits, feel them move through you and into the ground dissipating like water into the soil beneath everything.

Thank you for spending time with this zine and with the terminal.  If this sparked a desire to know more about the command line and computers, here are some resources I recommend:

~~> ◊ext-link["https://techlearningcollective.com"]{Tech Learning Collective} is the best school, hands down, to learn about how to work with your computer.  They offer regular workshops and longer courses, and have amazing, self-directed ◊ext-link["https://techlearningcollective.com/foundations/"]{foundation courses} about doing more with the command line.

~~> Your computer already has a grimoire for the command line, calling no attention to itself until you ask for it.  For any spirit, you can read their 'manual page' with ◊cmd{man spirit}, e.g. ◊cmd{man cat} or ◊cmd{man ls} or even ◊cmd{man man}.  Each spirit has its own page in this continuously growing manual.  While the language is a bit awkward, the information is solid, and the level of helpfulness in the terminal is real inspiring to me.

~~> If you haven't already, come out to our ◊ext-link["https://solarpunk.cool/magic/computer/club"]{solarpunk magic computer club}, which goes every Sunday at 11am(New Zealand Time, so it might Saturday in the evening for you non-kiwis).  This zine sprang from one of the club days, so it gives a good flavour to what we generally about!  It happens online at ◊ext-link["https://tv.solarpunk.cool"]{tv.solarpunk.cool}.

That's all.  Yah!
◊call{echo "Goodbye, friend"}


