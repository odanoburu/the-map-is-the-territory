#lang pollen

◊define-meta[title]{List what's here}

Let's get a better sense of exactly where we are.  

In your terminal type:
◊call{ls}

You should see something like:
◊response{Desktop Documents Downloads Library Pictures}

This is a list of every folder and file in your home directory.

You likely see some familiar names.  In this place, even your desktop is just a folder with some files.  You are seeing its essence: a bit of text on a black(or white) screen.

◊spirit{ls}
