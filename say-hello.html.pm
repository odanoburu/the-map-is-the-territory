#lang pollen
◊define-meta[title]{Say Hello}


Type the following into your terminal and press enter: ◊call{hello}

You should see this response:
◊response{bash: hello: command not found}

It's a bit of a bizarre response to your politeness, but notice that it is not saying you typed anything wrong.  There is no error here.  Rather, you typed what it understood to be a command, and it looked for the meaning of this command and could not find it.

In other words: the computer doesn't understand what you're saying.

On the command line, you don't speak to the computer in English.  Instead you use a technical creole called bash◊fn[1]. We call a bash phrase a ◊strong{command}, and the trick to the terminal is learning enough commands to move around this space easily.

In the terminal, your main tools are eloquence and  metaphor.  You are constrained only by what you don't know how to express.  At the beginning this will be quite a lot. But language, like your heart, wants to expand, and as you spend more time here, a shared language will grow between you and the computer, spoken in a dialect entirely your own.

Very exciting! Let's learn a couple key phrases.

◊|footnotes|

◊fndef[1]{To be fully accurate, there are many types of shells, each their own language, with Bash being but one of them.  Your computer may be running one called zsh too.  Bash is the most common, and likely what you're seeing, so I didn't wanna complicate thigns too much.}

