#lang pollen

◊define-meta[title]{Echo a Blessing}

With this next step, we move from simple commands to spell work.  With the command line, as in any magical practice, we are listening to the flow of energy and directing it to our will.  Commands, like all words, are streams of energy.

To show this we will craft a blessing to leave in our altar.

Think of something you'd like to remember. A nice word or phrase, some message to yourself and this space.

When ready, echo it into your computer.  Type:
◊call{echo "welcome to my altar!"}

It returns:
◊response{welcome to my altar!}

As expected, it repeats this blessing below the prompt.  To be more specific, though, echo took the blessing and sent it whichever direction we asked. Since we gave it no direction, it echoed it to the terminal screen.  

Hit the up arrow (to go back to our previous echo command) and let's add direction to the end of these words:
◊call{echo "welcome to my altar!" >> blessing}

This time, the message is not printed below. The sigil ◊cmd{>>} directs the output given by a command and writes it to the end of whatever file you name◊fn[1].  If that file does not exit, it will manifest it.

To see, type:
◊call{ls}

And you will now see:
◊response{blessing}

your altar has a blessing :)◊fn[2].

We can read our blessing with the trusty, flexible command ◊cmd{cat}.

◊spirit{cat}

In your terminal type:
◊call{cat blessing}

For myself, i see:
◊response{welcome to my altar!}

If you feel so moved, extend the blessing with another nice phrase.
type;
◊call{echo "the internet is animist" >> blessing}
and then
◊call{cat blessing}

For me, I see:
◊response{welcome to my altar!}
◊response{the internet is animist}

Your nice note grows with each directed echo.  I find this very cool!

◊|footnotes|

◊fndef[1]{Not to be confused with its more forceful sibling ◊code{>} which will ◊em{overwrite} the file with your words.}

◊fndef[2]{I never get over this, that the command line is words charged with words, energy paused in place as a name.  This feeling gets lost in the ui, where all files have the look of computer copy paper.  When it's just text, I find it overwhelmingly beautiful.  That's all, a lil bit of author earnestness!}
