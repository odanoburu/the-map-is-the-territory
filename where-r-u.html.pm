#lang pollen

◊define-meta[title]{where are you? (pwd)}

We know now how we see each other, next let's figure out where we are.

In your terminal, type:
◊call{pwd}

You should see something like:
◊response{Users/YourUserName}

(Mine gives me ◊code{Users/Zach})

In the terminal, the location of a file or a person is also just a bit of text, with each folder and subfolder (also called directories and sub-directories) separated by a ◊code{/}.  Right now we are in what's known as your ◊em{home directory}.

This doesn't give us too much information, yet, outside of a cute notion that we are home. 

◊spirit{pwd}


