#lang pollen

◊define-meta[title]{Change Direction}

Asking about things is fun, but we've been still for too long. Let's mooooove.

In your terminal, type:
◊call{cd Desktop}

The terminal won't say anything, it will just do as you ask, maybe flashing the prompt to let you know something changed.  What's changed is that you are now in your Desktop instead of your Home.

◊code{cd} stands for 'change directory' and is the main way you'll move through your computer.

This exchange shows the focus and attention your computer gives during a terminal session. It wasted no time with chatter-- no "Gotcha! let's change directories.  Whoof, we made it! You are now in the Desktop."  It just gave a flash of a nod, did as directed, and waited quietly for your next request.  This focus can be disorienting -- it can feel as if nothing is happening, or that you typed something wrong -- but it becomes liberating, to know you are in such tight flow with this machine instead of working against it.

You've also likely noticed a quality in all these commands: they're incredibly short!  Bash is not a spoken language, nor even much of a written one.  It is a typing language.  Its words have evolved to be as easy as possible to type while still holding meaning.  Some bash words have no letters at all -- one word you'll use aaalllll the time is ◊code{~}, which means 'home directory'.

We can use past commands to verify that we've moved and get more comfy with the language.  Type ◊cmd{pwd} and see that we are truly in the Desktop.  Type ◊cmd{ls} to see all the files on your desktop.

◊spirit{cd}
