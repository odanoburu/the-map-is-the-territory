#lang pollen

◊define-meta[title]{Make a Spells Folder}

Let's create a space in this altar for our spellwork, and let's make it a ◊em{hidden space}. type:
◊call{mkdir .spells}
(Note the . before spells.)
and then:
◊call{ls}

you will only see:
◊response{blessing}

anything that starts with a dot will be hidden from normal view.  However, we can ask ls to show us all, even the hidden things.  We do this by adding a modifier to our invocation. type:
◊call{ls -a}

you will see it all◊fn[2]:
◊response{. .. .spells blessing}

As you learn this language and become more familiar with these command spirits, the computer will open up and reveal more to you.

Try:
◊call{ls -a ~}


There's a number of hidden files in your home directory, invisible until you knew how to ask for them.

This continual surprise, of something being ever present but invisible until called upon, is a regular experience in the command line.◊fn[3]

◊|footnotes|

◊fndef[1]{These are often called 'flags'}

◊fndef[2]{about the ◊code{.} and ◊code{..} : These are special symbolic links that represent your current directory and the directory one above, respectively.  From the altars dir, you could type ◊cmd{ls ..} and it would list everything in the home folder.  These symbolic links are especially handy for moving up your files, a la ◊cmd{cd ..}}

◊fndef[3]{One fun one to try is ◊cmd{uname -a}.  Now you'll get the computer's operating systen name and a whoooole bunch more.  Computer didn't wanna open up unless you asked it to.}
