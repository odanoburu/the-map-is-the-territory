#!/bin/bash
WEBPAGE="${SOLARPUNKCOOLDIR}homepage/zines/map-is-the-territory"
raco pollen reset && raco pollen render
echo "Moving rendered pages to ${WEBPAGE}"
cp *.html $WEBPAGE
cp -r aesthetic $WEBPAGE
solarpunkPush
