# The Map Is the Territory

![screenshot of homepage with dithered logo saying 'the map is the territory'](aesthetic/screenshot-homepage.png)

https://solarpunk.cool/zines/map-is-the-territory

This is a zine about the command line, a place of magic and animism inside your computer.

The zine was written by [Zach Mandeville](https://coolguy.website) and [Angelica Blevins](https://angblev.com), using the language [pollen](https://docs.racket-lang.org/pollen/)

It sprang from the solarpunk magic computer club, a sunday club for people into magic computers and solarpunk.

Check it out and more at https://solarpunk.cool

## About This Repo

This is the source material for the zine at [solarpunk.cool/zines](https://solarpunk.cool/zines/map-is-the-territory).  With pollen, you write with a markup language you define as you go along (form = content = media = message), and then use the language Racket to set how this markup gets translated into your output content (in my case, html, but hopefully pdf soon too!)

What this means is that you can read the zine entirely in text files, and in a certain sense it was designed to be read this way.  Or it is at least aesthetically interesting.

If you do this, I recommend doing it using pollen's server, cos it has nice font and a pleasant mint green colour.

### Reading the text with pollen

You will need to install racket and pollen, following [pollen's installation guide](https://docs.racket-lang.org/pollen/Installation.html)

Then, clone down and enter this repo
```shell
git clone https://gitlab.com/cool-zines/the-map-is-the-territory.git
cd map-is-the-territory
```

From within the repo, start up the server
```shell
raco pollen start . 42069
```
You can then visit the zine at `localhost:42069`

It looks like this:
![pollen server screenshot](aesthetic/pollen-server.png)

If you click the `in` link for any file, you'll get the original text version.  if you click the file's name, it'll show you the rendered html.  

The file [pollen.rkt](pollen.rkt) has all the rendering functions we used.
Throughout the text you'll see references to command spirits, they are defined in our [spirits.rkt](spirits.rkt)

The best part of exploring the zine this way, is you now you got pollen installed on yr computer and you can make yr own zine with it, which i _hiiiighly_ recommend.

(if you do this, please send it to me.)

## Thanks

- Thanks to all the friends at solarpunk magic computer club for creating the space for stuff like this.
- Thanks to Matthew Butterick for creating Pollen, and all the pollen folks answering questions at https://github.com/mbutterick/pollen-users
- Thanks to the patch of trees right above the bus tunnel at the top of pirie street for being a good spot to lean your back against and look out at the water as you write up a zine.
- Thanks to [tech learning collective](https://techlearningcollective.com) for introducing me to the command line.
- Thanks to [Lumnos](https://lumnos.bandcamp.com/), [charmer](https://charmermusic.bandcamp.com/album/ivy), [katie dey](https://katiedey.bandcamp.com/album/solipsisters), [skirts](https://skirts.bandcamp.com/album/night-walks-in-love), [Alrakis](https://selfmutilationservices.com/album/echoes-from-eta-carinae), [Obsequaie](https://listen.20buckspin.com/album/the-palms-of-sorrowed-kings), and [Laura Luna Castillo](https://cudighirecords.bandcamp.com/album/folksonomies) for being the sountrack to this zine.






