#lang pollen
◊define-meta[title]{whoru?}

Now, we should ask the computer about itself.  There isn't a ◊cmd{whoru} command, but there ◊em{is} one as awkwardly phrased.

We can ask the computer "you name?" shortened to hip command line slang.

In the terminal, type:
◊call{uname}

It will return:◊fn[1]
◊response{Darwin (or Linux...or maybe DOS?)}

In other words, it just gives you its operating system name. Maybe it feels the stickers you placed on it, and maybe its absorbed your keystrokes into its own sense of self, but it doesn't know how to express this. The computer doesn't know itself the way you know it.  There's a gap of understanding, a mouth filled with unsaid words.  Many terminal commands, you'll come to understand, feel like one-word midwest emo mixtapes.

◊spirit{uname}


◊|footnotes|

◊fndef[1]{What you see depends on what type of computer you have.  On a mac, you'll see "Darwin".  On Linux you'll see "Linux".}

