#lang pollen

◊define-meta[title]{Cast the Circle}

Before we get actively typing, let's settle into this space and get it ready.

The command line is quiet and introspective, and this spell will be the same!  If you are unable to be in a private space, like you're reading this zine in the bustling cafe of a major city library, you can still do this spell◊fn["public"]!

The spell is mostly a visualization. Read through it all first, then move through it with your eyes closed.

◊divider{}

Have your terminal open, as large as you can get it.  Now, close your eyes.  Feel this space and consciously feel your breath slow down.  Focus on inhaling from the stomach, then exhaling soft and slow through the nose.

Visualize the space around you now.  Think of what is behind you, in front of you, to your left and right.  What is above you and below you?  Without opening your eyes, bring to focus every part of the room you are in.  Are you sitting?  What does the chair look like?  What color are the walls in this space?  Is there a window?  What color of light is coming through it?

Focus your attention to the space behind you.  Picture it with bright clarity and attentive detail.  Without opening your eyes, direct your gaze to the largest item in this direction, picturing every detail.  With an exhale, watch as this object dissolves into pure white light.  The shape of it is still there, still distinct among all the other items in this space, but its matter is pure light.  Envision everything else in this space behind you, exactly as they are, slowly transformed into this light.  Pick each object one by one, focus on it, and watch it dissolve into light.  Continue doing this until the entire space behind you is shining and shapeless.

When you're ready, shift your internal gaze to the left. Envision every detail there in accurate, crystalline detail.  One by one, focus and watch each object dissolve into that pure white energy.  Do this until everything to your left is part of the same shapeless light. Then turn your attention and, slowly, envision this happening to everything to the right of you. Picture it all in full detail as if your eyes were open.

When ready, Envsion everything in front of you.  Picture your computer, and the terminal screen open and watch as they dissolve into the same light.  What is the computer resting on? Watch as it dissolves into light too, along with everything surrounding it.

Let the surrounding light grow until it is everything above and below you. Feel it radiate all around you. Feel the chair beneath you dissolve into the light, feel your feet, your legs, your chest, your body, your arms, your head dissolve into this light.  Without opening your eyes, look around this room.  Look at every object where you left it, but now indistinguishable in this glow. Focus your gaze, patiently watching the pure white light in its unchanging radience.

Now, listen to the room you are in.  You will hear music.  Where in this place does the music come from?  Perhaps you hear voices, the sounds of outside.  Where is this outside in this place you find yourself now?  Where does the sound live?

You are remembering the words of this spell, remembering how it ends.  Where do the words live?

Feel yourself in this room.  No part is distinct, but it still feels recognizable.  What is the presence of the room when it has no shape?

Stay in this space until the music ends. Then, when you're ready, open your eyes.

◊divider{}

When you feel ready to cast the circle, Press 'start' below, then close your eyes and move through it.

◊div[#:id "casting-tools"]{
◊button[#:id "casting-countdown"]{start}
◊em[#:class "hidden" #:id "song-link"]{This song is 'Asleep on the Train' by Radical Face, ◊ext-link["https://store.radicalface.com/117922/Ghost-Vinyl"]{off the album Ghost}}
}


◊|footnotes|

◊fndef["public"]{You will want to wear headphones.}



