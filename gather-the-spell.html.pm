#lang pollen

◊define-meta[title]{Gather the Spell}


The spell is hidden as fragments of four lines in the source code of a group of websites, marked by the sigil of a comet: ◊code{~~*}.

◊h2{begin}

Let's begin, with a prelude from solarpunk.cool

In your terminal, type:
◊call{curl -L solarpunk.cool/magic/begin | grep "~~*" -A2 | tail -2 >> begin}

(This has been our most complex invocation◊fn[1] and needs to be typed exactly to work properly, so double check what you typed before pressing enter.)

We can check it worked with our trusty cat:
◊call{cat begin}

You will see:
◊response{Earth water fire air}◊response{within me all things are there}

◊h2{Earth}
Next, we gather from the earth and coolguy.website:
◊call{curl -L coolguy.website/earth | grep "~~*" -A2 | tail -2 >> earth}

◊h2{Water}
Gather from water and angblev.com:
◊call{curl -L angblev.com/water | grep "~~*" -A2 | tail -2 >> water}

◊h2{Fire}
Gather from fire and our home solarpunk.cool
◊call{curl -L solarpunk.cool/magic/fire | grep "~~*" -A2 | tail -2 >> fire}

◊h2{Air}
Gather from air and apileof.rocks
◊call{curl -L apileof.rocks | grep "~~*" -A2 | tail -2 >> air}

◊h2{End}
We end the spell by returning to solarpunk.cool
◊call{curl -L solarpunk.cool/magic/end | grep "~~*" -A2 | tail -2 >> end}

◊h2{Check it worked}
Lastly, type:
◊call{ls}

you should see;
◊response{air begin earth end fire water}

We've gathered the parts.  Now we can cast.

◊|footnotes|

◊fndef[1]{Here's a breakdown of what we're doing:

◊cmd{curl -L solarpunk.cool/magic/begin} => curls the source code from that url, the ◊code{-L} means "follow redirects" which just ensures we get to the actual if the server needs to redirect us (e.g. move from http to https).

◊cmd{ | grep "~~*" -A2} :: Then, we ◊em{pipe} that code to ◊cmd{grep} which searches for the line with "~~*".  Adding ◊cmd{-A2} means we return the line with the comet and the 2 lines After.

◊cmd{ | tail -2} :: We take these three lines of text and pipe it to our spirit ◊cmd{tail}.  We haven't used tail yet, but what it does is take a stream of text and a number, N, and returns the last N lines from that stream.  Here we are asking for the last 2 lines, which means we'd keep the two lines of the spell and let the comet disappear.

◊cmd{ >> begin} :: lastly, we take these two lines and condense them into the word begin, saved as a file in your ◊code{.spells} folder.

That's a lot!  but it's also so little!  So much with so little!}
